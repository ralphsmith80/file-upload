/// <reference types="Cypress" />
import { uploadFile } from './utils'
import { FILE_TYPES } from '../../../src/constants'
import { MESSAGES } from '../../../src/components/upload/constants'

const SELECTORS = {
  cardAction: '[data-cy=CardAction]',
  cardContent: '[data-cy=CardContent]',
  deleteFile: '[data-cy=DeleteFile]',
  fileCard: '[data-cy=FileCard]',
  fileUpload: '[data-cy=UploadButton]',
  fileUploadError: '[data-cy=UploadError]',
  fileImport: '#file-import',
  fileFilter: '[data-cy=SearchInput]',
}

describe('File List', () => {
  beforeEach(() => {
    cy.visit('/')
  })

  context('upload file card', () => {
    it('uploads a png file', () => {
      cy.get(SELECTORS.fileUpload).click()
      uploadFile('keepify.png', FILE_TYPES.png, SELECTORS.fileImport)
      cy.get(SELECTORS.fileCard).its('length').should('be.gt', 0)
    })
    it('uploads a jpg file', () => {
      cy.get(SELECTORS.fileUpload).click()
      uploadFile('keepify.jpg', FILE_TYPES.jpg, SELECTORS.fileImport)
      cy.get(SELECTORS.fileCard).its('length').should('be.gt', 0)
    })
    it('handles files size too large', () => {
      cy.get(SELECTORS.fileUpload).click()
      uploadFile('tooLarge.jpg', FILE_TYPES.jpg, SELECTORS.fileImport)
      cy.get(SELECTORS.fileUploadError).should(
        'contain.text',
        MESSAGES.MAX_FILE_SIZE
      )
    })
    it('handles files of wrong type', () => {
      cy.get(SELECTORS.fileUpload).click()
      uploadFile('keepify.svg', FILE_TYPES.svg, SELECTORS.fileImport)
      cy.get(SELECTORS.fileUploadError).should(
        'contain.text',
        MESSAGES.INVALID_FILE_TYPE
      )
    })
  })

  context('load files cards', () => {
    it('has files loaded', () => {
      cy.get(SELECTORS.fileCard).its('length').should('be.gt', 0)
    })
    it('has delete button for all cards', () => {
      cy.get(SELECTORS.fileCard).each((element, idx) => {
        cy.get(SELECTORS.cardAction).eq(idx).should('contain.text', 'Delete')
      })
    })
  })

  context('filters files cards', () => {
    it('filters files loaded', () => {
      const text = 'keep'
      cy.get(SELECTORS.fileFilter).type(text)
      cy.get(SELECTORS.cardContent).should('contain.text', text)
    })
    it('filters files to no results', () => {
      const text = '!@#$#$%^$%^&*'
      cy.get(SELECTORS.fileFilter).type(text)
      cy.get(SELECTORS.fileCard).should('not.exist')
    })
  })

  context('delete files cards', () => {
    it('delete all files', () => {
      cy.get(SELECTORS.fileCard).its('length').should('be.gt', 0)
      cy.get(SELECTORS.fileCard).each((element) => {
        element.find(SELECTORS.deleteFile).click()
      })
    })
    it('has no cards', () => {
      cy.get(SELECTORS.fileCard).should('not.exist')
    })
  })
})
