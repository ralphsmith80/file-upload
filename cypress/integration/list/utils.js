export const uploadFile = (fileName, fileType = '', selector) => {
  // this is the path in the fixtures folder
  const url = '/list'

  cy.get(selector).then((subject) => {
    cy.fixture(`${url}/${fileName}`, 'base64')
      .then(Cypress.Blob.base64StringToBlob)
      .then((blob) => {
        const el = subject[0]
        const testFile = new File([blob], fileName, { type: fileType })
        const dataTransfer = new DataTransfer()
        dataTransfer.items.add(testFile)
        el.files = dataTransfer.files

        cy.get(selector).trigger('change', { force: true }) // Without it Cypress does not understand the file was actually uploaded
      })
  })
}
