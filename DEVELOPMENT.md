# Upload file

An application to upload an host public files up to 10 MB in size.

## How to Run

Run locally after cloning:

1. install dependencies `npm ci`
1. start the app locally `npm start`

Run Unit tests:

1. `npm test`

Run E2E tests:

1. `npm e2e:open`

## Thoughts

### UI

The UI is simplistic and the requirements note CSS as a point of judgment so I'll keep it simple. However the UI could easily be improved by using a library like material-ui which would solve all the component needs for this and provide the necessary responsive layout components through it's wrapped version of JSS. In short that's what I would do if I wanted to release this to real customers. That would off load a lot of basic effort, it would look great, and it would be easier to maintain.

### API

The API is simplistice but I know if I write my own I'm going to waste a lot of time since that's not something I do on a regular basis. If I did go that route my first reach would be for [python-eve](https://docs.python-eve.org/en/stable/) which is an amazing library based on Flask that is database agnostic via extensions but uses MongoDB by default. I've made contributions to their [sqlalchemy](https://github.com/pyeve/eve-sqlalchemy) extension in the past. That library never got the love I personal felt it deserved so it might be a bit dated now and before going that route I would research possible Node options. That might be faster since it's been sometime since I've developed in python.

My next thought is to use [json-server](https://github.com/typicode/json-server) to mock a rest api. I know using the `db.json` file I could have real pesistance of the file name but probably not the real file data without more effort on writing the data to json file in text format. I also have concerns of how I would protect this data based on size and type. I know I can write custome endpoint with this method but doing so generally removes the ability to persist the `db.json` file automatically and I don't thing that's effort well spent. This approach would still meet the requirements since there is no option to download the file and I could just note the improvements but I think there's a better way that's less effort.

Firebase is what I'm thinking for this test project. I have a lot of firebase experiences so standing up a project won't take much time. The storage API and machine learning aspects are the only two parts of firebase I haven't used yet so this will be a good opportunity to cross one of those off the list. Going this route means this will not be a RESTful service but there is no requirment to make it restful. Additionally there's no requirement to make the api public. However, both of those could easily be solved by proving three firebase functions that are restful and interact with the storage api. I'm not planning on going that route but I'll note that it's possible and wouldn't be too difficult to add. I can demonstrate how REST APIs work with functions on another project I have it's a point of interest.

### Other Considerations

#### CI/CD

CI/CD that is some that in my opinion is always over complicated. Typically as a result of not using the right tools. I plan to set that up for this project as it would be something I would set up for any project I intend to release and iterate on. This is easy with gitlab pipelines and firebase.

#### Commit Stucture (future changelog)

Commits will follow the [conventional commits](https://www.conventionalcommits.org/en/v1.0.0/) standard so versioning and changelog generation can be automated. Again something I would do any project I intend to release and iterate on. I will note the versioning in this context is typically irrelavent as I can demonstate when we discuss. The commit sha is sufficent for versioning purposes. However, the changelog is not something that should be over looked.

#### Security

Security is of course top of mind. Since there's no authN/authZ for this application there UX problems. Anyone could delete anyone elses uploads. Again since I'm using firebase this could easily be addressed in the `storage.rules` file if to enforce authZ if authN was added. With that said I'm only focusing on the frontend needs. Off the top of my head these are the concerns I would be looking to address:

- [XSS](https://owasp.org/www-community/attacks/xss/)
- [CSRF](https://owasp.org/www-community/attacks/csrf)
- [XSHM](<https://owasp.org/www-community/attacks/Cross_Site_History_Manipulation_(XSHM)>)
- [Clickjacking](https://owasp.org/www-community/attacks/Clickjacking)

My initial thought on that ...

I don't see any possibility for an XSS attack as we're using React and react automatically escapes inputs. Unless I intentially did something with `dangerouslysetinnerhtml` we should be okay there.

Regarding CSRF there's a possiblity a bad actor could upload a neferious file that has a URL to invoke bad things. We should be covered by the default same policy origin implemented by modern browsers. If downloading files was added as a feature this could still lead to a problem but to enable that you would need to [explicity enable CORS](https://firebase.google.com/docs/storage/web/download-files#cors_configuration) for firebase storage and consider this is a public upload system that's probably a bad idea.

XSHM is not one I'm not very familiar with but as I understand is similar to XSS or CSRF by manipulating a conditional redirect. We should be clear in this case since we do not have any conditional redirects.

The defense against clickjacking is to utilise CSP to prevent content from other domains. This should prevent anyone sneaker than me from uploading and image with bytecode that creates a frame from which can introduce a clickjacking attack. This is enabled on firebase but can be modified if necessary.

#### Storage Space and Flexibiliy

Since I chose to use Firebase storage for this solution we will have a horizontal flexibility that I don't need to think about but I did want to note that it was something I was aware of as a concern. The free tier provides provides the following [amount of storage](https://firebase.google.com/pricing):

- 5 GB stored - ~2,500 high-res photos
- 30 GB transferred - ~15,000 high-res photos
- 2,100,000 ops - ~210,000 uploads & 1,890,000 downloads

#### Performance

There's certainly room for improvment here. I didn't implement any pagination but that would be a neccessary first step.

I didn't debounce the search field. That would be another probably neccessity when there are a large number of file being search through.

## Process of development

1. Setup initial project using CRA
1. Add firebase to make it real and to have a real storage API
1. Add a bunch of info to this README to capture my thoughts on building this
   _phew!_ that was a a lot :)
1. Add CI/CD (worked the first time!)
   ![](./docs/gitlab-deployment.png)
   ![](./docs/firebase-deployment.png)
1. Start writing code
   1. Add [absolute imports](https://create-react-app.dev/docs/importing-a-component/#absolute-imports)
   1. Add [automatic formatting](https://create-react-app.dev/docs/setting-up-your-editor/#formatting-code-automatically)
   1. play with the storage api to figure out how it works
   1. itentify the security rules to make data public and limit size to 10MB - [storage docs](https://firebase.google.com/docs/storage)
   1. encapsulate storage usage with functions
   1. add components Button, FileButton, Card, Input, Label (Typography), Error - this is where unit tests should exist
   1. add container components Main, List, Search, Upload - this is where integration tests should exist
      _Note:_ at this point there been a lot of basic code written. I would not typically build all of this library type of code without some tests but for the purposes of ensuring I have time to get something function done I'm pushing forward with more code. Typically I would want some tests around the library features. Although I did have to fix the App.js test to ensure the CI/CD pipeline still worked. Also I'll note the components are not completely styled yet.
   1. Address the responsive layout with Grid. I'm making this choice because the upload button is on top of the search in the smaller view. If it were the other way around I would just use flexbox. Also this could be done with floats and media queries. i.e. float upload left when greater than the small window definition.
   1. More styling to make things look and flow correctly. Main additions include a theme to share some styling vars. There is partial duplication in CSS via a var I'm only using basic CSS and the duplication is required to enable consistent media queries.
      _Note:_ I added lodash to get a uniqueId utility. I'm using this to set a unique key on react elements when mapping over card data to render Card components.
   1. make upload real
   1. make list real
      _Note:_ I added numeraljs to easily and consistently format the size of the file. I also added a couple of custom hooks to interact with the storage api with a hook interface. These could be cleaned up and combined into one but I'm keeping them distinct so I can keep moving forward with required functionality.
   1. make delete real
   1. make search real
   1. all feature functionality is complete
      _Note:_ there are a number of things that I'm not happy about with the current implementation. Rendering is to impactful. I didn't separate the styling between the List, Search, and Upload components enough so the width of the entire app shrinks when the list width is small. I used margins with flexbox on the list which doesn't look very good when there are multiple rows. I don't like that there's a mix of inline CSS and CSS files. I would prefer to use something like JSS or CSS modules only to keep it consistent.
      The firebase storage worked great for everything except search. There might be a better way to work around that than the API encapsulation I did. Worst case you could definelty do that with a firebase http function and do all the work on the backend. My API encapsulation basically represents that without actually the backend code to do that.
   1. realized I'm missing the Total counts and sizes. Also found a fix for the flexbox marning issue using [gap](https://coryrylan.com/blog/css-gap-space-with-flexbox)
      _Note:_ the firebase storage API does not include the metaData by default. I broke that out initally but decided to roll it up under the same `list` call to simplify the rollup of total size. I also removed some of the rerendering based on `isLoading` so the displaying of data wasn't so jumpy.
1. Test (cypress/jest) - not following official TDD
   1. Previously updated the App.test.js jest test.
   1. Add cypress for e2e tests.
   1. covered the following use cases
      1. upload - the tests are in place but there is an issue with cypress/firebase I was unable to solve yet. The file type is being submitted correctly as a `File` but firebase is throwing an error when executed via crypess.
         `Firebase Storage: Invalid argument in put at index 0: Expected Blob or File.`
         This means you need to ensure there are files pre-uploaded (from the fixtures folder) prior to executing the e2e tests until the issue can be resolved.
      1. upload errors
      1. list render/load
      1. delete
      1. search
   1. The [cypress base url](./cypress.json) is set to `localhost:3000` but it can be updated to run against the production version `https://file-upload-e42f3.web.app/` and that would be the approach in the CI/CD pipeline.
1. Outstanding Gaps
   1. The UI could be improved to make the rendering smoother. This would be trivial with more work on the styling and state systems.
   1. I have the storeage rules in place to validate the file against the requirements on the backend but there were some syntax bugs which I didn't get a chance to correct. I believe the issue was the regex match on `.jpg|.png` which was is [documented](https://firebase.google.com/docs/reference/security/database/regex) but isn't playing nice.
   1. Add standard version or semantic release to generate changelogs. I"ve already been using conventional commits so this will work out of the box with the addition of one of those tools.
   1. Run test against production environment and publish test reports. I like to post these in a gitlab page so they can be viewed after every run. This can be done for jest coverage reports and cypress coverage reports.
   1. Use cypress mocks to enable offline development without the need for a real back end. I intentionally skip doing a lot of unit test on the library components so that I could focus on end-to-end tests for the required functionality. I believe this is far more valuable but library component should have unit test coverage.
   1. Fix for firebase `storage.put` when invoked via cypress.
