import React from 'react'
import { Main } from 'components/main/Main'
import { THEME } from 'components/theme/theme'
import './App.css'

const styles = {
  maxWidth: `${THEME.appMaxWidgth}px`,
  margin: `${THEME.spacing * 2}px`,
}
function App() {
  return (
    <div className="App" style={styles}>
      {/* We only have main but there could be a header and footer here or more */}
      <Main />
    </div>
  )
}

export default App
