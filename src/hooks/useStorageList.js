import { useState, useEffect } from 'react'
import storageApi from 'api/storage/storage'
import { STORAGE_MESSAGES } from '../constants'

export const MESSAGES = {
  ...STORAGE_MESSAGES,
  CATCH_ALL: 'Good luck!',
}

export const useStorageList = ({ searchTerm = '', force = false }) => {
  const [error, setError] = useState(null)
  const [response, setResponse] = useState([])
  const [isLoading, setLoading] = useState(true)

  useEffect(() => {
    setLoading(true)
    storageApi
      .list(searchTerm)
      .then((data) => {
        setError(null)
        setResponse(data)
        setLoading(false)
      })
      .catch((e) => {
        setError({ message: MESSAGES[e.code] || MESSAGES.CATCH_ALL })
        setLoading(false)
      })
    // ideally we would cancel any inflight request to prevent state updates on unmounted components
    return () => {}
  }, [searchTerm, force])

  return { isLoading, response, error }
}
