/* istanbul ignore file */
import firebase from 'firebase/app'
// Required for side-effects
import 'firebase/app'
import 'firebase/storage'

const FIREBASE_CONFIG = {
  apiKey: 'AIzaSyB40Z1s-Dfr8dOuVkOUIQsi5R-e9jE9EY0',
  authDomain: 'file-upload-e42f3.firebaseapp.com',
  databaseURL: 'https://file-upload-e42f3.firebaseio.com',
  projectId: 'file-upload-e42f3',
  storageBucket: 'file-upload-e42f3.appspot.com',
  messagingSenderId: '165933032317',
  appId: '1:165933032317:web:f3d76d0c3f965dcff4a214',
}

firebase.initializeApp(FIREBASE_CONFIG)

export const storage = firebase.storage()

export default firebase
