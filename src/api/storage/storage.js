/* istanbul ignore file */
import { storage } from 'api/firebase/firebase'

const STORAGE_REF = storage.ref()

class Storage {
  /**
   * Deletes a file.
   * @param {storage_ref} ref - A storage file reference.
   * @return {undefined} - `undefined`.
   * @throws {error} - The error when accessing firebase.storage.
   */
  delete(ref) {
    return ref.delete().then(
      (res) => {
        console.log('Deleted successful!')
        return res
      },
      (e) => e
    )
  }
  /**
   * Provides metaData for a file.
   * @param {storage_ref} ref - A storage file reference.
   * @return {object} - The meta data for a file.
   * @throws {error} - The error when accessing firebase.storage.
   */
  metaData(ref) {
    return ref.getMetadata().then(
      (metaData) => {
        ref.metaData = metaData
        return metaData
      },
      (e) => {
        console.error(e)
        throw e
      }
    )
  }
  /**
   * Lists all files.
   * @param {string} [searchTerm] - The name of a file.
   * @return {array} - The list of files.
   * @throws {error} - The error when accessing firebase.storage.
   */
  list(searchTerm) {
    // INFO: Firebase storage is a bit limiting to satisfy the search requirement.
    // Searching will require a full list load and then filtering :(.
    // INFO: We're encapsulate the storage API call for meta data with the fetch for an item.
    // This will simplify the calculating the total size of all the files.
    const searchTermRegex = new RegExp(searchTerm, 'gi')
    return STORAGE_REF.listAll()
      .then(
        (res) => {
          let items = []
          let metaDataPromises = []
          res.items.forEach((item) => {
            const { name } = item
            if (!searchTerm) {
              items.push(item)
              metaDataPromises.push(this.metaData(item))
            } else if (name.match(searchTermRegex)) {
              items.push(item)
              metaDataPromises.push(this.metaData(item))
            }
          })
          return Promise.all(metaDataPromises).then(
            (metaDataCollection) => items
          )
        },
        (e) => {
          console.error(e)
          throw e
        }
      )
      .then(
        (itemsWithMetaData) => {
          return itemsWithMetaData
        },
        (e) => {
          console.error(e)
          throw e
        }
      )
  }
  /**
   * Provides metaData for a file.
   * @param {file} file - A File object.
   * @return {object} - A snapshot of the task.
   * @throws {error} - The error when accessing firebase.storage.
   */
  upload(file) {
    return STORAGE_REF.child(file.name)
      .put(file)
      .then((res) => {
        console.log('Upload successful!')
        return res
      })
  }
}

const storageApi = new Storage()
export default storageApi
