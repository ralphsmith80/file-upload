import React, { useState } from 'react'
import { FileInput } from 'components/button/FileInput'
import { Error } from 'components/error/Error'
import storageApi from 'api/storage/storage'
import { MESSAGES } from 'components/upload/constants'
import { FILE_TYPES, MAX_FILE_SIZE } from './../../constants'
import './Upload.css'

/* istanbul ignore next */
const styles = {
  section: {
    display: 'flex',
    flexDirection: 'column',
  },
}

const processFile = (file) => {
  // There might more things I want to do here that could require tests
  // pulling this out makes doing that easier
  return storageApi.upload(file)
}

export const Upload = ({ onChange, style }) => {
  const [error, setError] = useState('')

  return (
    <section className="UploadSection" style={{ ...styles.section, ...style }}>
      <FileInput
        data-cy="UploadButton"
        className="UploadButton"
        fullWidth
        handleFile={(evt) => {
          const file = evt.target.files[0]

          if (!file) {
            return null
          }
          if (file.type !== FILE_TYPES.jpg && file.type !== FILE_TYPES.png) {
            setError(MESSAGES.INVALID_FILE_TYPE)
            return null
          }
          if (file.size > MAX_FILE_SIZE) {
            setError(MESSAGES.MAX_FILE_SIZE)
            return null
          }
          // TODO: set disabled state until file is uploaded
          // With firebase you can make this work with offline by checking the progress and restarting progress
          // but that's for another day.
          processFile(file)
            .then((res) => {
              onChange(res)
            })
            .catch((e) => setError(MESSAGES[e.code] || MESSAGES.CATCH_ALL))
        }}
        InputProps={{ accept: '.jpg,.png' }}
        onClick={() => setError('')}
      >
        UPLOAD
      </FileInput>
      <Error data-cy="UploadError">{error}</Error>
    </section>
  )
}

Upload.defaultProps = {
  onChange() {},
  style: {},
}

export default Upload
