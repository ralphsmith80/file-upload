import numeral from 'numeral'
import { BYTE_FORMAT, MAX_FILE_SIZE, STORAGE_MESSAGES } from './../../constants'

export const MESSAGES = {
  ...STORAGE_MESSAGES,
  INVALID_FILE_TYPE: 'File must be of type jpg or png',
  CATCH_ALL: 'Good luck!',
  MAX_FILE_SIZE: `File must be smaller than ${numeral(MAX_FILE_SIZE).format(
    BYTE_FORMAT
  )}`,
}
