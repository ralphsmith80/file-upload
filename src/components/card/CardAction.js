import React from 'react'

/* istanbul ignore next */
const styles = {
  alignSelf: 'flex-end',
}

export const CardAction = (props) => <div style={styles} {...props}></div>

CardAction.defaultProps = {
  'data-cy': 'CardAction',
}

export default CardAction
