import React from 'react'

/* istanbul ignore next */
const styles = {
  border: '1px solid black',
  padding: '1rem 1rem',
  display: 'flex',
  justifyContent: 'space-between',
}

// This could be more perscriptive by requiring CardAction and CardContent props
// but I'm leaving it compleltely open for children although in this case we will
// expect the children to be CardAction and CardContent
export const Card = ({ style, ...rest }) => (
  <div style={{ ...styles, ...style }} {...rest}></div>
)

Card.defaultProps = {
  style: {},
}

export default Card
