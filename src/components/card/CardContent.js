import React from 'react'
import { THEME } from 'components/theme/theme'

/* istanbul ignore next */
const styles = {
  marginRight: `${THEME.spacing}px`,
}

export const CardContent = ({ style, ...rest }) => (
  <div style={{ ...styles, ...style }} {...rest}></div>
)

CardContent.defaultProps = {
  'data-cy': 'CardContent',
  style: {},
}

export default CardContent
