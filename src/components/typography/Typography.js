import React from 'react'

// This is just an example of encapsulating viriants for our Typography system
// Using React.createElement inline with appropriate props would probably be a better way.
// INFO: I specifiy children here specifially to correct an a11y warning. It's not a real error
// because the children get passed as required by the props but the linter doesn't know that.
const Variants = ({ children, variant, ...rest }) => {
  switch (variant) {
    case 'h2':
      return <h2 {...rest}>{children}</h2>
    case 'header':
      return <h4 {...rest}>{children}</h4>
    case 'body':
    default:
      return <p {...rest}>{children}</p>
  }
}
export const Typography = (props) => <Variants {...props} />

/* istanbul ignore next */
Typography.defaultProps = {
  variant: 'body',
}

export default Typography
