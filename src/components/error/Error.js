import React from 'react'

/* istanbul ignore next */
const styles = {
  color: 'red',
}

export const Error = (props) => <span style={styles} {...props}></span>

export default Error
