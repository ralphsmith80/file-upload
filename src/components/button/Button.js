import React from 'react'

/* istanbul ignore next */
const styles = {
  button: {
    border: '1px solid black',
    backgroundColor: 'lightblue',
    padding: '0.5rem 2rem',
    textAlign: 'center',
    userSelect: 'none',
  },
  fullWidth: {
    width: '100%',
  },
}

// INFO: the fullWidth prop would be the ideal way to go but since I didn't make a hook
// for media queries it's not going to work well. I'm using a media query to address this later.
export const Button = ({ fullWidth, style, ...rest }) => (
  <span
    style={
      fullWidth
        ? { ...styles.button, ...styles.fullWidth, ...style }
        : { ...styles.button, ...style }
    }
    {...rest}
  ></span>
)

Button.defaultProps = {
  fullWidth: false,
  style: {},
}

export default Button
