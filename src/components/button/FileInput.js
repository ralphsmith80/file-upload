import React from 'react'
import { Button } from 'components/button/Button'

const styles = {
  input: {
    display: 'none',
  },
  label: {
    display: 'inherit',
  },
}
export const FileInput = ({ handleFile, InputProps, ...rest }) => (
  <React.Fragment>
    <input
      id="file-import"
      onChange={/* istanbul ignore next */ (e) => handleFile(e)}
      type="file"
      style={styles.input}
      {...InputProps}
    />
    <label htmlFor="file-import" style={styles.label}>
      <Button {...rest} />
    </label>
  </React.Fragment>
)

/* istanbul ignore next */
FileInput.defaultProps = {
  handleFile() {},
  InputProps: {},
}

export default FileInput
