import React from 'react'
import numeral from 'numeral'
import { uniqueId } from 'lodash'
import { Error } from 'components/error/Error'
import { FileCard } from 'components/list/FileCard'
import { Typography } from 'components/typography/Typography'
import { useStorageList } from 'hooks/useStorageList'
import { BYTE_FORMAT } from './../../constants'
import './List.css'

/* istanbul ignore next */
const styles = {
  cardMargin: {
    // emulation for `gap` in all modern browsers
    margin: 'var(--gap) 0 0 var(--gap)',
  },
  listContent: {
    display: 'flex',
    flexWrap: 'wrap',
    // Just `gap` is enough in the newest bowsers but not every yet.
    // gap: '7.5px',
    // emulation for `gap` in all modern browsers
    '--gap': '7.5px',
    margin: 'calc(-1 * var(--gap)) 0 0 calc(-1 * var(--gap))',
    width: 'calc(100% + var(--gap))',
  },
}

export const List = ({ onChange, searchTerm, style, updateListMemo }) => {
  const { error, isLoading, response: cards } = useStorageList({
    searchTerm,
    force: updateListMemo,
  })

  const totalSize = cards.reduce(
    (total, card) => total + card?.metaData?.size || 0,
    0
  )

  return (
    <section style={style}>
      {!error && (
        <header className="ListHeader">
          <Typography variant="h2">{`${cards.length} documents`}</Typography>
          <Typography>{`Total size: ${numeral(totalSize).format(
            BYTE_FORMAT
          )}`}</Typography>
        </header>
      )}
      <div style={styles.listContent}>
        {!isLoading && error && <Error>{error.message}</Error>}

        {!error &&
          cards.map((card, index) => {
            return (
              <FileCard
                card={card}
                key={uniqueId()}
                onDelete={onChange}
                style={styles.cardMargin}
              />
            )
          })}
      </div>
    </section>
  )
}

List.defaultProps = {
  onChange() {},
  searchTerm: '',
  style: {},
  updateListMemo: '',
}

export default List
