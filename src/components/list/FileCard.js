import React, { useState } from 'react'
import numeral from 'numeral'
import { Card, CardAction, CardContent } from 'components/card'
import { Error } from 'components/error/Error'
import { Typography } from 'components/typography/Typography'
import { Button } from 'components/button/Button'
import storageApi from 'api/storage/storage'
import { BYTE_FORMAT, STORAGE_MESSAGES } from './../../constants'
import './FileCard.css'

/* istanbul ignore next */
const styles = {
  card: {
    width: '345px',
  },
}

/* istanbul ignore next */
export const MESSAGES = {
  ...STORAGE_MESSAGES,
  CATCH_ALL: 'Unable to delete file',
}

export const FileCard = ({ card, onDelete, style, ...rest }) => {
  const [deleteError, setDeleteError] = useState(null)
  const { metaData } = card

  return (
    <Card className="FileCard" style={{ ...styles.card, ...style }} {...rest}>
      <CardContent>
        {deleteError && <Error>{deleteError}</Error>}
        <Typography variant="header">{card.name}</Typography>
        <Typography>{numeral(metaData.size).format(BYTE_FORMAT)}</Typography>
      </CardContent>
      <CardAction>
        <Button
          data-cy="DeleteFile"
          onClick={() => {
            setDeleteError(null)
            storageApi.delete(card).then(
              () => onDelete(card),
              (e) => setDeleteError(MESSAGES[e.code] || MESSAGES.CATCH_ALL)
            )
          }}
        >
          Delete
        </Button>
      </CardAction>
    </Card>
  )
}

FileCard.defaultProps = {
  card: {},
  'data-cy': 'FileCard',
  onDelete() {},
  style: {},
}

export default FileCard
