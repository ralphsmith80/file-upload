import React from 'react'
import { Input } from 'components/input/Input'
import { THEME } from 'components/theme/theme'

/* istanbul ignore next */
const styles = {
  // INFO: variable cap on the max width for the search based on the application max width
  maxWidth: `${THEME.appMaxWidgth - THEME.appMaxWidgth / 2}px`,
  padding: '0.5rem 2rem',
  width: '100%',
}

export const Search = ({ onChange, style }) => (
  <section style={{ ...styles.section, ...style }}>
    <Input
      data-cy="SearchInput"
      onChange={(evt) => onChange(evt.target.value)}
      placeholder="Search documents..."
      style={styles}
      type="text"
    />
  </section>
)

Search.defaultProps = {
  onChange() {},
  style: {},
}

export default Search
