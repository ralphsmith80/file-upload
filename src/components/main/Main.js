import React, { useState } from 'react'
import { uniqueId } from 'lodash'
import { Upload } from 'components/upload/Upload'
import { List } from 'components/list/List'
import { Search } from 'components/search/Search'
import './Main.css'

/* istanbul ignore next */
const styles = {
  main: {
    display: 'grid',
    gridGap: '1rem',
    gridTemplateAreas: `
        "upload"
        "search"
        "list"
      `,
  },
  upload: {
    gridArea: 'upload',
  },
  search: {
    gridArea: 'search',
  },
  list: {
    gridArea: 'list',
  },
}

export const Main = () => {
  // I'm using basic react state to manage application state. There are much better tools for this.
  // This will not be the more performant option. As it will rerender the entier app.
  // At a minimum we could consider memoizing some of these calls.
  const [searchTerm, setSearchTerm] = useState('')
  // I'm using this to force update the list when the searchTerm has not changed but the data has.
  // This is due to a delete or an upload. I don't like this approach but I want to get some tests in.
  // Might come back to this if there's time and thing about a better way.
  const [updateListMemo, setUpdateListMemo] = useState(uniqueId())

  return (
    <main className="main" style={styles.main}>
      <Upload
        onChange={() => {
          setSearchTerm('')
          setUpdateListMemo(uniqueId())
        }}
        style={styles.upload}
      />
      <Search
        onChange={(value) => setSearchTerm(value)}
        style={styles.search}
      />
      <List
        onChange={() => {
          setSearchTerm('')
          setUpdateListMemo(uniqueId())
        }}
        searchTerm={searchTerm}
        style={styles.list}
        updateListMemo={updateListMemo}
      />
    </main>
  )
}

export default Main
