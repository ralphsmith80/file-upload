# Ralph - September 27, 2020

## Installation

```
npm ci
```

### Run for Development

```
npm start
```

### Run Unit Tests

```
npm test
```

### Run E2E Tests

```
npm e2e:open
```

_Note_: There is currently an [issue](#bug-fixes) with uploading files in these tests. The workaround is to manually upload 1 or more files before running the tests if a file does not already exist.

### Production Environment

https://file-upload-e42f3.web.app/

### Repository

https://gitlab.com/ralphsmith80/file-upload/

## Security

Security is of course top of mind. Since there's no authN/authZ for this application there could be UX problems. Anyone could delete anyone elses uploads. Again since I'm using firebase this could easily be addressed in the `storage.rules` file by enabling authZ if authN was added. I looked to address the concerns:

- [XSS](https://owasp.org/www-community/attacks/xss/)
- [CSRF](https://owasp.org/www-community/attacks/csrf)
- [XSHM](<https://owasp.org/www-community/attacks/Cross_Site_History_Manipulation_(XSHM)>)
- [Clickjacking](https://owasp.org/www-community/attacks/Clickjacking)

I don't see any possibility for an XSS attack as we're using React and react automatically escapes inputs. Unless I intentially did something with `dangerouslysetinnerhtml` we should be okay there.

Regarding CSRF is would be possible for a bad actor to upload a neferious file which contains a URL to invoke bad things. We should be covered by the default same policy origin implemented by modern browsers. If downloading files was added as a feature this could still lead to a problem but to enable that you would need to [explicity enable CORS](https://firebase.google.com/docs/storage/web/download-files#cors_configuration) for firebase storage and considering this is a public upload system that's probably a bad idea.

XSHM is not one I'm not very familiar with but as I understand it is similar to XSS or CSRF by manipulating a conditional redirect. We should be clear in this case since we do not have any conditional redirects.

The defense against clickjacking is to utilise CSP to prevent content from other domains. This is set by the firebase system to protect us but if we wanted to change that it could be configured. This should prevent anyone sneaker than me from uploading and image with bytecode that creates a frame to introduce a clickjacking attack.

Issues that I'm not addressing include SQL injection attacks. The firebase storage API is a noSQL model which changes the scope for that kind of attack. It would have to be another type of attack. I'm pushing the boundaries of my knowledge at this point. I'd have to do more research to identify other potential issues.

## Improvements

I ended up building a fullblown production system but there are still lot's of improvements to be made. From that standpoint it's in a great spot for iteration.

### UI

The UI could be improved to make the rendering smoother. This would be trivial with a better styling and state systems. Personally I would choose something similar to JSS and Redux but not necessarily those. If I want to keep it closer to platform native then I would leverage CSS modules and React context. Both things I chose not to use in this project.

### Security

I have the [storeage rules](./storage.rules) in place to validate the file against the requirements on the backend but there were some syntax bugs which I didn't get a chance to correct. I believe the issue was the regex match on `.jpg|.png` which is [documented](https://firebase.google.com/docs/reference/security/database/regex) but wasn't playing nice.

### Code Quality

Add standard version or semantic release to generate changelogs. I"ve already been using conventional commits so this will work out of the box with the addition of one of those tools. That another quick addition to ensure we have clear communicaton around the code.

### Testing

Run test against production environment and publish test reports. I like to post coverage reports in a gitlab page so they can be viewed after every run. This can be done for jest coverage reports and cypress coverage reports.

Use cypress mocks to enable offline development without the need for a real back end. I intentionally skipped doing a lot of unit test on the library components so that I could focus on end-to-end tests for the required functionality. I believe this is far more valuable but library component should have unit test coverage. I'm not discounting the value of unit tests just noting that unit test have more value when implemented at the right level.

### Bug Fixes

There is an issue with the uploading files in the [e2e tests](#run-e2e-tests). The coverage is there to illustrate the necessity of testing this aspect but I've have note been able to itentify a fix. The issue is during the `storage.put` to firebase when invoked via cypress. We create a file of type [File](https://developer.mozilla.org/en-US/docs/Web/API/File) using one of our [fixtures](./cypress/fixtures/list/filter.png) which appears to be working correctly but is not correctly itentifed as a file of type `File` by firebase.

## Libraries

I tried to keep the list small as possible while still implemention a realistic solution. I'll group the us of each in a related category.

### Code Qaulity

- husky
- link-staged
- prettier

I used each of these to enable automatic code formatting and ensure code consistency.

### API/Storage

- firebase

Firebase was used to leverage the provided storage API. I encapsulated it's use in the [storage.js](./src/api/storage/storage.js). I did this to provide a necessary abstarction and create the API. The underlying storage could be swapped out with out changing the UI provided the interface is adhered to.

### Utility

- lodash
- numeral
- create react app (react-scripts)

I used lodash for the [`uniqueId`](https://lodash.com/docs/4.17.15#uniqueId) method to ensure React keys were unique. In truth I could have used the ID's from the real data for keys as well and removed the need for this.

Numeral was used to properly fromat the Byte information (size) on files.

Create React App was used for scaffolding the application.

### Testing

- cypress
- jest

Cypress was used to implement end-to-end tests. It could also be used for unit tests.

Jest came for free with the Create React App setup. I've used Jest a lot in the past but I would favor just using Cypress.

## API

For measure I have a lot more thougt around the API in the [DEVELOPMENT](./DEVELOPMENT.md#api) but I'll focus on the choosen solution here. I choose to write and ecapsulation on top of the firebase storage API. On the surface it supported everything required out of the box. However, there were some limitations with the ability query for an item. I was able to keep that abstraction in the API encapsulation a more performant option would be ideal with a large dataset.

The biggest limitation right now is missing pagination. It is supported by the firebase API but I did not have time to make that abstaction and include it here.

Other thought around this would be offline support. Firebase provides offline support for many of there APIs and storage falls in that category although it's not as seemless as the firestore implementation. You could leverage the projess of the upload to restart if connection was every lost. This is likey the most practicle use for offline. Persisting a hug list of files on you mobile device for offline viewing might not be realistic but it another possibility.

Finally as noted in the [DEVELOPMENT](./DEVELOPMENT.md#api) document I did not see any requirements to provide a RESTful service. With that sad my API implimentation is more aken to an SDK where you would install a library to manage you're requests. Although I did note in the [DEVELOPMENT](./DEVELOPMENT.md#api) doc how you could put a RESTful api on top of this. This is still an asyncrhonous service and I'll note the support methods below.

For referance all methods exist in [storage.js](./src/api/storage/storage.js). Each of these will include JSDoc syntax as well.

### Storage:list

Lists all files.
Accepts an optional `searchTerm` parameter.
Returns a Array of files.
Returns a filter Array of files if optional `searchTerm` is provided.
Returns an empty Array if no files exist.
Throws an Error if storage scheme is invalid. This will be one of [STORAGE_MESSAGES](./src/constants.js) as defined by the [torage docs](https://firebase.google.com/docs/storage/web/handle-errors).

```
/**
 * Lists all files.
 * @param {string} [searchTerm] - The name of a file.
 * @return {array} - The list of files.
 * @throws {error} - The error when accessing firebase.storage.
 */
```

### Storage:delete

Deletes a file.
Accepts a required [storage reference](https://firebase.google.com/docs/storage/web/create-reference) parameter.
Returns `undefined`.
Throws an Error if storage scheme is invalid. This will be one of [STORAGE_MESSAGES](./src/constants.js) as defined by the [torage docs](https://firebase.google.com/docs/storage/web/handle-errors).

```
/**
 * Deletes a file.
 * @param {storage_ref} ref - A storage file reference.
 * @return {undefined} - `undefined`.
 * @throws {error} - The error when accessing firebase.storage.
 */
```

### Storage:metaData

Provides metaData for a file.
Accepts a required [storage reference](https://firebase.google.com/docs/storage/web/create-reference) parameter.
Returns {object} - The meta data for a file.
Throws an Error if storage scheme is invalid. This will be one of [STORAGE_MESSAGES](./src/constants.js) as defined by the [torage docs](https://firebase.google.com/docs/storage/web/handle-errors).

```
/**
 * Provides metaData for a file.
 * @param {storage_ref} ref - A storage file reference.
 * @return {object} - The meta data for a file.
 * @throws {error} - The error when accessing firebase.storage.
 */
```

### Storage:upload

Uploads a file to the storage system.
Accepts a required [File](https://developer.mozilla.org/en-US/docs/Web/API/File) parameter.
Returns {object} - A snapshot of the task.
Throws an Error if storage scheme is invalid. This will be one of [STORAGE_MESSAGES](./src/constants.js) as defined by the [torage docs](https://firebase.google.com/docs/storage/web/handle-errors).

For clarity a snapshot will contain the information noted [here](https://firebase.google.com/docs/storage/web/upload-files) and summarized below.

- bytesTransferred - `Number` - The total number of bytes that have been transferred when this snapshot was taken.
- totalBytes `Number` - The total number of bytes expected to be uploaded.
- state - `firebase.storage.TaskState` - Current state of the upload.
- metadata - `firebaseStorage.Metadata` - Before upload completes, the metadata sent to the server. After upload completes, the metadata the server sent back.
- task - `firebaseStorage.UploadTask` - The task this is a snapshot of, which can be used to `pause`, `resume`, or `cancel` the task.
- ref - `firebaseStorage.Reference` - The reference this task came from.

```
/**
 * Provides metaData for a file.
 * @param {file} file - A File object.
 * @return {object} - A snapshot of the task.
 * @throws {error} - The error when accessing firebase.storage.
 */
```

## Other notes

If you actually get this far I have a lot more reading for you :). I annotated the [DEVELOPMENT](./DEVELOPMENT.md#api) during this process to capture in works my initial thought and development process. I refereced this above so you may have already read through it. If so ... I have nothing more. I'm spent :).
